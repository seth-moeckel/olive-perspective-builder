package jobmgmt

import khttp.get
import khttp.put
import khttp.post

import java.net.ConnectException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import org.json.JSONObject

data class Job(
    val id: String,
    val perspectiveName: String, 
    val rulescript: String,
    val ruleGlobs: List<String>
)

class JobQueue(val hostUrl: String) {
    var loopInterval: Long = 5000

    fun getNextJob(): Job {
        val res = get("$hostUrl/jobs/next")
        if (res.statusCode != 200) {
            throw Exception("failed to complete getNextJob request: status $res.statusCode, body: $res.text")
        }

        return Job(
            id = res.jsonObject.get("id") as String,
            perspectiveName = res.jsonObject.get("perspectiveName") as String,
            rulescript = res.jsonObject.get("rulescript") as String,
            ruleGlobs = res.jsonObject.get("rlueGlobs") as List<String>
        )
    }

    fun getJobById(id: String): Job {
        val res = get("$hostUrl/jobs/$id")
        if (res.statusCode != 200) {
            throw Exception("failed to complete getJobById request: status $res.statusCode, body: $res.text")
        }

        return Job(
            id = res.jsonObject.get("id") as String,
            perspectiveName = res.jsonObject.get("perspectiveName") as String,
            rulescript = res.jsonObject.get("rulescript") as String,
            ruleGlobs = res.jsonObject.get("rlueGlobs") as List<String>
        )
    }

    fun startJob(id: String) {
        val res = put(
            "$hostUrl/jobs/$id/status",
            json = mapOf("status" to "started")
        )
        if (res.statusCode != 200) {
            throw Exception("failed to complete startJob request: status $res.statusCode, body: $res.text")
        }
    }

    fun completeJob(id: String) {
        val res = put(
            "$hostUrl/jobs/$id/status",
            json = mapOf("status" to "complete")
        )
        if (res.statusCode != 200) {
            throw Exception("failed to complete completeJob request: status $res.statusCode, body: $res.text")
        }
    }

    fun failJob(id: String, cause: String) {
        val res = put(
            "$hostUrl/jobs/$id/status",
            json = mapOf("status" to "fail", "error" to cause)
        )
        if (res.statusCode != 200) {
            throw Exception("failed to complete failJob request: status $res.statusCode, body: $res.text")
        }
    }

    fun sendJobKeepAlive(id: String) {
        val res = post("$hostUrl/jobs/$id/keepalive")
        if (res.statusCode != 200) {
            throw Exception("failed to complete sendJobKeepAlive request: status $res.statusCode, body: $res.text")
        }
    }

    fun withInterval(interval: Long): JobQueue {
        this.loopInterval = interval
        return this
    }

    fun watch(callback: (Job) -> Unit) {
        callback(Job("null", "null", "null", listOf("null")))
    }
}
//     fun watch(callback: (Job) -> Unit) {
//         // used to mark job status in finally block
//         val failMessage = null
//         val job = null
//         val chanKeepalive = Channel<Boolean>()

//         while (true) {
//             try {
//                 job = this.getNextJob()
//                 if (job != null) {
//                     // indicate job start
//                     this.startJob(job.id)

//                     // coroutine concurrently keeps job alive
//                     GlobalScope.launch {
//                         val run = true
//                         while(run) {
//                             // send job keep alive
//                             // jobs timeout after 5 minutes of no keepalives

//                             this.sendJobKeepAlive(job.id)
//                             Thread.sleep(5000)

//                             if (chanKeepalive.receive() == true) {
//                                 run = false
//                             }
//                         }
//                     }

//                     // run job callback
//                     callback(job)
//                 }
//             } catch (e: ConnectException) {
//                 // indicate job failure
//                 failMessage = e.message
//                 println(String.format("watch loop: failed to complete job: %s error: %s", job.id, e.message)
//             } finally {
//                 // stop keepalive channel
//                 chanKeepalive.send(true)

//                 if (failMessage != null) {
//                     // indicate job failure
//                     this.failJob(job.id, failMessage)
//                 } else {
//                     // indicate job completion
//                     this.completeJob(job.id)
//                 }
//             }

//             // block and reset failed status/chan
//             Thread.sleep(loopInterval)

//             failMessage = null
//             chanKeepalive = Channel<Boolean>()
//         }
//     }
// }