package model.dataconv

import org.apache.spark.api.java.JavaRDD 
import org.apache.spark.api.java.JavaSparkContext
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row
import org.apache.spark.sql.RowFactory
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.DataTypes
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType

fun toDataFrame(session: SparkSession, input: List<String>): Dataset<Row> {
    val ctx = JavaSparkContext.fromSparkContext(session.sparkContext())
    val items = ctx.parallelize(input)
    
    val rdd = items.map { RowFactory.create(it) }
    val schema = DataTypes.createStructType(arrayOf<StructField>(DataTypes.createStructField("filename", DataTypes.StringType, false)))

    return session.sqlContext().createDataFrame(rdd, schema)
}
