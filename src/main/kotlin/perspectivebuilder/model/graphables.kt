package model

import model.types.Union

data class Node(val id: String, val typeName: String, val fields: Map<Any, Any>)
data class Edge(val src: String, val dst: String, val typeName: String, val fields: Map<Any, Any>)

typealias Graphable<Node, Edge> = Union.U2<Node, Edge>
