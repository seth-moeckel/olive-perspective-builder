package egress.neo4j

import java.io.Serializable
import org.neo4j.driver.v1.AuthTokens
import org.neo4j.driver.v1.Driver
import org.neo4j.driver.v1.GraphDatabase
import org.neo4j.driver.v1.Session
import org.neo4j.driver.v1.StatementResult
import org.neo4j.driver.v1.Transaction

import model.Node
import model.Edge

data class Config(val url: String, val user: String, val password: String): Serializable

class Neo4jDatabase(config: Config) {
    var session: Session

    init {
        session = GraphDatabase.driver(config.url, AuthTokens.basic(config.user, config.password)).session()
    }

    fun execQuery(session: Session, query: String, params: Map<String, Any>): Any {
        val tx: Transaction = session.beginTransaction()
        val result: StatementResult = tx.run(query, params)

        tx.success()
        tx.close()

        return result
    }

    fun writeNode(node: Node) {
        val query =
"""
MERGE (n:${node.typeName} {hash: "${node.id}", ${toCypher(node.fields)}})
RETURN n
"""

        this.execQuery(this.session, query, mapOf())
    }

    fun writeEdge(edge: Edge) {
        val query =
"""
MATCH (a), (b) WHERE a.hash = "${edge.src}" AND b.hash = "${edge.dst}"
CREATE (a)-[r:${edge.typeName}]->(b)
RETURN r
"""

        this.execQuery(this.session, query, mapOf())
    }
}