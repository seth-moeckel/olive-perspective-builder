package egress.neo4j

import org.jruby.RubySymbol
import kotlin.collections.Map.Entry

fun toCypher(input: Map<Any, Any>): String {
    return input.map{ 
        val entry: Entry<Any, Any> = it
        var key = entry.key

        when (key) {
            is RubySymbol -> key = key.asJavaString()
            is String -> key = key as String
        }

        "${key}: \"${entry.value}\""
    }.reduce{ a, b -> a + ", $b" }
}