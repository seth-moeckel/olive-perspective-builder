package perspectivebuilder

data class Config(
    val appName: String,
    val s3Url: String,
    val s3Bucket: String,
    val s3ApiKey: String,
    val s3Secret: String,
    val neo4jUrl: String,
    val neo4jUser: String,
    val neo4jPassword: String
)

data class NeoConfig(
    val neo4jUrl: String,
    val neo4jUser: String,
    val neo4jPassword: String
)

fun importEnv(appName: String): Config {
    // TODO add validation for env vars

    return Config(
        appName,
        System.getenv("S3_URL"),
        System.getenv("S3_BUCKET"),
        System.getenv("S3_APIKEY"),
        System.getenv("S3_SECRET"),
        System.getenv("NEO4J_URL"),
        System.getenv("NEO4J_USER"),
        System.getenv("NEO4J_PASSWORD")
    )
}
