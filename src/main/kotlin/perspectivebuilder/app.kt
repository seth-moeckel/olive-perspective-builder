package perspectivebuilder

import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaSparkContext
import kotlin.reflect.KClass
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.Row

import jobmgmt.JobQueue
import jobmgmt.Job
import ingress.s3.Bucket
import ingress.s3.Config as S3Config
import egress.neo4j.Config as NeoConfig
import egress.neo4j.Neo4jDatabase
import rulesEngine.ruby.exec
import model.dataconv.toDataFrame
import model.Node
import model.Edge

val example = 
"""
apply_to "/files/*/*_test.json", "/medstar/*/!.png"

patient = Node :patient, {
    name: key("#{${'$'}input.first} #{${'$'}input.last}"),
    ssn: key(${'$'}input.ssn),
    dob: ${'$'}input.dob,
    mrn: ${'$'}input.mrn
}

if ${'$'}input.address
    address = Node :address, {
        :canonical => key(${'$'}input.address)
    }

    patient.owns_with address, {
        :since => "some date"
    }
end

if ${'$'}input.insurance
    insurance_carrier = Node :insurance_carrier, {
        :id => key(${'$'}input.insurance.id),
        :name => ${'$'}input.insurance.carrier,
        :phone => ${'$'}input.insurance.phone,
        :website => ${'$'}input.insurance.website
    }

    if ${'$'}input.insurance.address
        address = Node :address, {
            :canonical => key(${'$'}input.insurance.address)
        }

        insurance_carrier.location_is_with address, {

        }
    end
end

${'$'}input.visits.each do |visit|
    physician = Node :physician, {
        :name => key(visit.physician),
        :department => visit.department
    }

    patient.has_visited physician
    visit.diagnoses.each do |visit_diag|
        diagnosis = Node :diagnoses, {
            :code => key(visit_diag.code),
            :description => visit_diag.description
        }

        patient.has diagnosis
    end
end
"""

fun main(args: Array<String>) {
    val config = importEnv("olive-perspective-builder")
    val s3Config = S3Config(config.s3Url, config.s3Bucket, config.s3ApiKey, config.s3Secret)
    val neoConfig = NeoConfig(config.neo4jUrl, config.neo4jUser, config.neo4jPassword)

    val conf = SparkConf()
            .setMaster("local")
            .setAppName("Olive Perspective Builder")

    val session = SparkSession.builder()
            .master("local[*]")
            .config(conf)
            .appName("Olive Perspective Builder")
            .getOrCreate()

    JobQueue("http://localhost:5001/jobs/next").withInterval(5000).watch {
        val fileList = Bucket(s3Config).listObjects()
        val fileListRdd: Dataset<Row> = toDataFrame(session, fileList)

        fileListRdd.foreachPartition {
            val neo = Neo4jDatabase(neoConfig)
            val s3 = Bucket(s3Config)

            while (it.hasNext()) {
                val row: Row = it.next()
                val filename: String = row.getAs("filename")

                println("info/ executing mapper against: $filename")
                
                val json = s3.downloadAsJSON(filename)
                val output = exec(example, json)
                
                output.forEach {
                    it.use(
                        { node: Node -> neo.writeNode(node) },
                        { edge: Edge -> neo.writeEdge(edge) }
                    )
                }
            }
        }
    }
}