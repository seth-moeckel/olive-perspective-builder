package ingress.s3

import java.io.InputStream
import java.io.ByteArrayInputStream
import java.io.Serializable

import org.apache.commons.io.IOUtils
import groovy.json.JsonSlurper;

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.ClientConfiguration
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.S3ObjectSummary
import com.amazonaws.SdkClientException
import com.amazonaws.services.s3.model.ListObjectsV2Request
import com.amazonaws.services.s3.model.ListObjectsV2Result
import com.amazonaws.services.s3.model.S3Object
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration

data class Config(
    val s3Url: String,
    val s3Bucket: String,
    val s3ApiKey: String,
    val s3Secret: String
): Serializable

class Bucket(config: Config) {
    var client: AmazonS3
    var config: Config

    init {
        val credentials = BasicAWSCredentials(config.s3ApiKey, config.s3Secret)
        val clientConfiguration = ClientConfiguration()
        clientConfiguration.setSignerOverride("AWSS3V4SignerType")

        this.client = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(config.s3Url, Regions.US_EAST_1.name))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(AWSStaticCredentialsProvider(credentials))
                .build();

        this.config = config
    }

    fun listObjects(): List<String> {
        return this.client.listObjects(this.config.s3Bucket).getObjectSummaries().map { it.getKey() }
    }

    fun downloadObject(filename: String): ByteArray {
        val file: S3Object = this.client.getObject(this.config.s3Bucket, filename)
        return IOUtils.toByteArray(file.getObjectContent())
    }

    fun downloadAsString(filename: String): String {
        return String(this.downloadObject(filename), Charsets.UTF_8)
    }

    fun downloadAsJSON(filename: String): Any {
        return JsonSlurper().parse(this.downloadObject(filename))
    }
}
