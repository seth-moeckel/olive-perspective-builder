package rulesEngine.ruby

val DSLPrescript =
"""
require "java"
require "json"
require "digest"

java_import "model.Node"
java_import "model.Edge"

${'$'}deeppurple_rule_applies_to_files = []
${'$'}output = []

def apply_to *globs
    ${'$'}deeppurple_rule_applies_to_files = globs 
end

def Node sym, fields
    return GNode.new(sym, fields)
end

class PKey
    def initialize(val)
        @val = val
    end

    def value
        @val
    end
end

def key val
    return PKey.new(val)
end

class GNode 
    def initialize(typeName, fields)
        @typeName = typeName

        key_fields = { :type => typeName }
        new_fields = {}

        fields.each do |key, val|
            if val.is_a? PKey
                new_fields[key] = val.value
                key_fields[key] = val.value
            else
                new_fields[key] = val
            end
        end

        hash = Digest::MD5.new
        hash << key_fields.to_json
        @id = hash.hexdigest

        ${'$'}output.push Node.new(@id, @typeName, new_fields)
    end

    def id
        @id
    end

    # method_missing is a magic method that creates
    # an edge for any method call that doesn't pre-exist
    # and has any number of arguments that are instances of GNode
    def method_missing(methodName, *args, &block)
        fields = {}
        methodNameEnding = methodName[-5..-1]
        if methodNameEnding == "_with" then
            methodName = methodName[0...-5]
            args.pop
        end

        args.each do |arg| 
            if not arg.instance_of? GNode then
                raise ArgumentError.new("Cannot create edge against non-Node destinations")
            end

            ${'$'}output.push Edge.new(@id, arg.id, methodName, fields) 
        end
    end
end

class MagicHash
    def initialize(hash)
        hash.each do |k,v|
            if v.is_a? Java::OrgApacheGroovyJsonInternal::LazyMap
                self.instance_variable_set("@#{k}", MagicHash.new(v))
                self.class.send(:define_method, k, proc{self.instance_variable_get("@#{k}")})
            elsif v.is_a? Hash
                self.instance_variable_set("@#{k}", MagicHash.new(v))
                self.class.send(:define_method, k, proc{self.instance_variable_get("@#{k}")})
            elsif v.is_a? Array
                self.instance_variable_set("@#{k}", MagicArray(v))
                self.class.send(:define_method, k, proc{self.instance_variable_get("@#{k}")})
            elsif v.is_a? Java::JavaUtil::ArrayList
                self.instance_variable_set("@#{k}", MagicArray(v))
                self.class.send(:define_method, k, proc{self.instance_variable_get("@#{k}")})
            else
                self.instance_variable_set("@#{k}", v)
                self.class.send(:define_method, k, proc{self.instance_variable_get("@#{k}")})
            end
        end
    end
end

def MagicArray input
    new_arr = []
    input.each do |i|
        if i.is_a? Array
            new_arr.push MagicArray(i)
        elsif i.is_a? Java::JavaUtil::ArrayList
            new_arr.push MagicArray(i)
        elsif i.is_a? Java::OrgApacheGroovyJsonInternal::LazyMap
            new_arr.push MagicHash.new(i)
        elsif i.is_a? Hash
            new_arr.push MagicHash.new(i)
        else
            new_arr.push i
        end
    end

    return new_arr
end

def MagicObject input
    if input.is_a? Java::OrgApacheGroovyJsonInternal::LazyMap
        return MagicHash.new(input)
    elsif input.is_a? Hash
        return MagicHash.new(input)
    elsif input.is_a? Array
        return MagicArray(input)
    else
        return input
    end
end

${'$'}input = MagicObject(${'$'}input)
"""
