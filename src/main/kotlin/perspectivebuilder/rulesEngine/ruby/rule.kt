package rulesEngine.ruby

import java.util.Collection
import javax.script.ScriptContext
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import javax.script.ScriptException

import model.Graphable
import model.Node
import model.Edge

fun exec(script: String, input: Any): List<Graphable<Node, Edge>> {
    val engine = ScriptEngineManager().getEngineByName("jruby")
    engine.getContext().setAttribute("\$input", input, ScriptContext.ENGINE_SCOPE)

    engine.eval("$DSLPrescript\n\n$script")
    val output = engine.getContext().getAttribute("\$output");

    if (!(output is Collection<*>)) {
        throw Exception("rule didn't return array!");
    }

    return output.map {
        when (it) {
            is Node -> Graphable.ofA(it)
            is Edge -> Graphable.ofB(it)
            else -> throw Exception("rule returned array containing unsupported types!")
        }
    } as List<Graphable<Node, Edge>>
}