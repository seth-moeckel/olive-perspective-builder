package rulesEngine

import java.nio.file.FileSystems
import java.nio.file.Paths

fun globMatch(glob: String, filename: String): Boolean {
    return FileSystems.getDefault().getPathMatcher("glob:$glob").matches(Paths.get(filename))
}

interface Rule {
    fun run() {}
}

class MatchBank {
    fun add(rule: Rule, vararg globs: String): MatchBank {
        return this
    }

    fun resolve(filename: String): Rule? {
        return null
    }
}